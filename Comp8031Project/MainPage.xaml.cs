﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Comp8031Project
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private const double MoveDistance = 5;
        private double _xPos = Window.Current.Bounds.Width / 2;
        private double _yPos = Window.Current.Bounds.Height / 2;
        public MainPage()
        {
            this.InitializeComponent();
            Circle.SetValue(Canvas.LeftProperty, _xPos);
            Circle.SetValue(Canvas.TopProperty, _yPos);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var circle = e.Parameter as Ellipse;
            if (circle == null) return;
            _xPos = (double)circle.GetValue(Canvas.LeftProperty);
            _yPos = (double)circle.GetValue(Canvas.TopProperty);
            Circle.SetValue(Canvas.LeftProperty, _xPos);
            Circle.SetValue(Canvas.TopProperty, _yPos);
        }

        private void BtnLeft_Click(object sender, RoutedEventArgs e)
        {
            Circle.SetValue(Canvas.LeftProperty, _xPos-= MoveDistance);
        }

        private void BtnUp_Click(object sender, RoutedEventArgs e)
        {
            Circle.SetValue(Canvas.TopProperty, _yPos -= MoveDistance);
        }

        private void BtnDown_Click(object sender, RoutedEventArgs e)
        {
            Circle.SetValue(Canvas.TopProperty, _yPos += MoveDistance);
        }

        private void BtnRight_Click(object sender, RoutedEventArgs e)
        {
            Circle.SetValue(Canvas.LeftProperty, _xPos += MoveDistance);
        }

        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(typeof(SecondPage), Circle);
        }
    }
}
