﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Sensors;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Comp8031Project
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SecondPage : Page
    {
        private const double TriggerDegree = 10;
        private const double MoveDistance = 5;
        private double _xPos;
        private double _yPos;
        public SecondPage()
        {
            this.InitializeComponent();
            var inclinometer = Inclinometer.GetDefault();

            if (inclinometer != null)
            {
                // Establish the report interval for all scenarios
                uint minReportInterval = inclinometer.MinimumReportInterval;
                uint reportInterval = minReportInterval > 16 ? minReportInterval : 16;
                inclinometer.ReportInterval = reportInterval;

                // Establish the event handler
                inclinometer.ReadingChanged += new TypedEventHandler<Inclinometer, InclinometerReadingChangedEventArgs>(ReadingChanged);
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var circle = e.Parameter as Ellipse;
            if (circle == null) return;
            _xPos = (double)circle.GetValue(Canvas.LeftProperty);
            _yPos = (double)circle.GetValue(Canvas.TopProperty);
            CircleSec.SetValue(Canvas.LeftProperty, _xPos);
            CircleSec.SetValue(Canvas.TopProperty, _yPos);
        }

        private void GoBackToMain(object sender, RoutedEventArgs e)
        {
            SecondFrame.Navigate(typeof(MainPage), CircleSec);
        }

        private async void ReadingChanged(object sender, InclinometerReadingChangedEventArgs e)
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                InclinometerReading reading = e.Reading;
                var pitchDegrees = reading.PitchDegrees;
                var rollDegrees = reading.RollDegrees;
                var yawDegrees = reading.YawDegrees;

                if (pitchDegrees > TriggerDegree) //move down
                    CircleSec.SetValue(Canvas.TopProperty, _yPos += MoveDistance);
                if (pitchDegrees < -TriggerDegree) //move up
                    CircleSec.SetValue(Canvas.TopProperty, _yPos -= MoveDistance);
                if (rollDegrees > TriggerDegree) //move right
                    CircleSec.SetValue(Canvas.LeftProperty, _xPos += MoveDistance);
                if (rollDegrees < -TriggerDegree) //move left
                    CircleSec.SetValue(Canvas.LeftProperty, _xPos -= MoveDistance);

                tbPitch.Text = String.Format("{0,5:0.00}", pitchDegrees);
                tbRoll.Text = String.Format("{0,5:0.00}", rollDegrees);
                tbYaw.Text = String.Format("{0,5:0.00}", yawDegrees);
            });
        }
    }
}
